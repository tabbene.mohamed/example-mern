const Student = require('../models/Student');

const getStudents = async (req, res) => {
  try {
    const students = await Student.find().lean();
    res.send(students);
  } catch (error) {
    console.error(error)
  }
};


const getStudent = async (req, res) => {
  try {
  const student = await Student.findById(req.params.id).lean();
  res.send(student);
  }
 catch (error) {
  console.error(error)
}
};


const deleteStudent = async (req, res) => {
  let { id } = req.params;
  try{
    await Student.findByIdAndDelete({ _id: id });
  res.send(id);
  }
  catch (error) {
    console.error(error)
  }
};

const addStudent = async (req, res) => {
  const student = new Student(req.body);
  try {
    await student.save();
    res.send(student);
  } catch (error) {
    console.error(error)
  }
};

const updateStudent = async (req, res) => {
  const id = req.params.id;
  const { name, grade } = req.body;
  try{
    await Student.findByIdAndUpdate({ _id: id },{name,grade});
   res.send(id);
  }
  catch (error) {
    console.error(error)
  }
};

module.exports = {
  getStudents,
  getStudent,
  deleteStudent,
  addStudent,
  updateStudent
}