const mongoose = require('mongoose');

const studentSchema = mongoose.Schema({
    name:String,
    grade:String
});

module.exports = mongoose.model('student',studentSchema);


