const express = require('express');
const {
  register,
  CreateToken,
  login
} = require('../controllers/user');

const router = express.Router();

router.post("/register",register);
router.post("/login",CreateToken,login);

module.exports = router;
