const express = require('express');
const {
  getStudents,
  getStudent,
  deleteStudent,
  addStudent,
  updateStudent
} = require('../controllers/student');

const router = express.Router();

router.get("/", getStudents);
router.get("/:id", getStudent);
router.delete("/:id", deleteStudent);
router.post("/", addStudent);
router.put("/:id", updateStudent);

module.exports = router;
