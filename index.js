const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config();


mongoose.connect(process.env.DB_CONNECT,(err,done)=>{
  if(err){
    console.log(err)
  }
  if(done){
    console.log('succes !!!!!')
  }
  })


const studentRouter = require('./routes/student');
const userRouter = require('./routes/user');

const app = express();
app.use(express.json());



app.use("/students", studentRouter);
app.use("/", userRouter);


const PORT = 5000;
app.listen(PORT, () => {
  console.log(`The server is running on port ${PORT}`);
});
